#define servo1 33 //servo1 ligado no pino digital 33
#include <DS1307.h>
DS1307 rtc(A0, A1);

void servo0graus();
void servo45graus();

void setup()
{
    rtc.halt(false);
    rtc.setDOW(FRIDAY);      //Define o dia da semana
    rtc.setTime(17, 6, 50);     //Define o horario
    rtc.setDate(07, 11, 2016);
    rtc.setSQWRate(SQW_RATE_1);
    rtc.enableSQW(true);
    Serial.begin(9600);

    pinMode(servo1, OUTPUT); //saída para o servo1
}

void loop()
{
    char *hora = rtc.getTimeStr();
    if (hora[0] == '1' && hora[1] == '7' && hora[3] == '0' && hora[4] == '7' || hora[0] == '1' && hora[1] == '7' && hora[3] == '0' && hora[4] == '9')
    {
        for(char i=0;i<100;i++) servo0graus();    //move o servo para a posição 0º por 2 segundos
        for(char i=0;i<100;i++) servo45graus();    //move o servo para a posição 90º por 2 segundos
    }
    {
        Serial.print("Hora : ");
        Serial.println(rtc.getTimeStr());
        Serial.print(" ");
        Serial.print("Data : ");
        Serial.print(rtc.getDateStr(FORMAT_SHORT));
        Serial.print(" ");
        Serial.println(rtc.getDOWStr(FORMAT_SHORT));
        delay(1000);
    }
}

void servo0graus()              //Posiciona o servo em 0 graus
{
    digitalWrite(servo1, HIGH);  //pulso do servo
    delayMicroseconds(600);      //0.6ms
    digitalWrite(servo1, LOW);   //completa periodo do servo
    for(int i=0;i<32;i++)delayMicroseconds(500);
}

void servo45graus()             //Posiciona o servo em 90 graus
{
    digitalWrite(servo1, HIGH);  //pulso do servo
    delayMicroseconds(1000);     //1.5ms
    digitalWrite(servo1, LOW);   //completa periodo do servo
    for(int i=0;i<19;i++)delayMicroseconds(500);
}
