$(document).ready(function() {
var max_fields      = 5; //maximum input boxes allowed
var wrapper         = $(".date-panel"); //Fields wrapper
var add_button      = $(".new-date-input"); //Add button ID

var input_date_panel = '<div class="col s12"><div class="card-panel brow-color date-panel"><input placeholder="Insira a hora desejada" type="datetime" class="center-align"><label class="center-align">Insira o horário no formato: 00:00:00</label><a href="#" class="remove_field"><i class="material-icons">delete</i></a></div></div>'


var x = 1; //initlal text box count
$(add_button).click(function(e){ //on add input button click
    e.preventDefault();
    if(x < max_fields){ //max input box allowed
        x++; //text box increment
        // $(wrapper).append(input_date_panel); //add input box
        $(wrapper).append('<div class="col s12"><div class="card-panel brow-color date-panel"><input id="inputDate'+x+'" placeholder="Insira a hora desejada" type="datetime" class="center-align"><label class="center-align">Insira o horário no formato: 00:00:00</label><a href="#" class="remove_field"><i class="material-icons">delete</i></a></div></div>'); //add input box
    }
});

$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
    e.preventDefault(); $(this).parent('div').remove(); x--;
})
});
