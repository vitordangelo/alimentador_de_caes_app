var timer = new Timer();
$('.startButton').click(function () {
    timer.start({precision: 'seconds'});
});
$('.pauseButton').click(function () {
    timer.pause();
});
$('.stopButton').click(function () {
    timer.stop();
});

timer.addEventListener('secondsUpdated', function (e) {
    $('#chronoExample .values').html(timer.getTimeValues().toString());
});
timer.addEventListener('started', function (e) {
    $('#chronoExample .values').html(timer.getTimeValues().toString());
});
