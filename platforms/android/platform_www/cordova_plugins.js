cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "id": "fr.drangies.cordova.serial.Serial",
        "file": "plugins/fr.drangies.cordova.serial/www/serial.js",
        "pluginId": "fr.drangies.cordova.serial",
        "clobbers": [
            "window.serial"
        ]
    },
    {
        "id": "cordova-plugin-splashscreen.SplashScreen",
        "file": "plugins/cordova-plugin-splashscreen/www/splashscreen.js",
        "pluginId": "cordova-plugin-splashscreen",
        "clobbers": [
            "navigator.splashscreen"
        ]
    },
    {
        "id": "cordova-plugin-statusbar.statusbar",
        "file": "plugins/cordova-plugin-statusbar/www/statusbar.js",
        "pluginId": "cordova-plugin-statusbar",
        "clobbers": [
            "window.StatusBar"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.3.0",
    "fr.drangies.cordova.serial": "0.0.7",
    "cordova-plugin-splashscreen": "4.0.0",
    "cordova-plugin-statusbar": "2.2.0"
};
// BOTTOM OF METADATA
});