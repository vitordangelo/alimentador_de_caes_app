$(".button-on").on('click',function() {
    var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
    $('#dog-icon').addClass('animated tada').one(animationEnd, function() {
        $(this).removeClass('animated tada');
    });
});

$(".button-off").on('click',function() {
    var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
    $('#dog-icon').addClass('animated zoomInUp').one(animationEnd, function() {
        $(this).removeClass('animated zoomInUp');
    });

    var beepOne = $("#huff")[0];
    beepOne.play();
});
