let time = setInterval(function(){
    update()
}, 1000);

function update() {
    let d = new Date();
    let hour = d.getHours();
    let minutes = (d.getMinutes()<10?'0':'') + d.getMinutes();
    let seconds = (d.getSeconds()<10?'0':'') + d.getSeconds();

    $('.wotch').text(hour + ':' + minutes + ':' + seconds);
}

function myStopFunction() {
    clearInterval(time);
    $('.wotch').css("color","#e63636");
}
